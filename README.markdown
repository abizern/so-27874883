An example to accompany the Stack Overflow question at
[http://stackoverflow.com/questions/27874883/add-multiple-subviews-to-nswindow-in-swift](http://stackoverflow.com/questions/27874883/add-multiple-subviews-to-nswindow-in-swift)

Adds multiple views created from a nib as subviews from a view.
