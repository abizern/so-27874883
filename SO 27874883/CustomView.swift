//
//  CustomView.swift
//  SO 27874883
//
//  Created by Abizer Nasir on 10/01/2015.
//  Copyright (c) 2015 Jungle Candy Software Ltd. All rights reserved.
//

import Cocoa

class CustomView: NSView {
    
    @IBOutlet weak var label: NSTextField!
    
    class func newCustomView() -> CustomView {
        let nibName = "CustomView"
        
        // Instantiate an object of this class from the nib file and return it.
        // Variables are explicitly unwrapped, since a failure here is a compile time error.
        var topLevelObjects: NSArray?
        let nib = NSNib(nibNamed: nibName, bundle: NSBundle.mainBundle())!
        nib.instantiateWithOwner(nil, topLevelObjects: &topLevelObjects)
        
        var view: CustomView!
        
        for object: AnyObject in topLevelObjects! {
   
            if let obj = object as? CustomView {
                view = obj
                break
            }
        }
        
        view.layerContentsRedrawPolicy = .OnSetNeedsDisplay
        
        return view
    }
    
    override var wantsUpdateLayer: Bool {
        return true
    }
    
    override func updateLayer() {
        self.layer?.backgroundColor = NSColor.redColor().CGColor
    }

}
