//
//  AppDelegate.swift
//  SO 27874883
//
//  Created by Abizer Nasir on 10/01/2015.
//  Copyright (c) 2015 Jungle Candy Software Ltd. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var contentView: NSView!
    @IBOutlet weak var window: NSWindow!
    
    var subviewNumber = 1
    
    override func awakeFromNib() {
        contentView.layerContentsRedrawPolicy = .OnSetNeedsDisplay
        contentView.layer?.backgroundColor = NSColor.greenColor().CGColor
    }


    @IBAction func addView(sender: AnyObject) {
        let size = CGSize(width: 100.0, height: 100.0)
        let origin = randomOriginInView(contentView, forViewOfSize: size)
        let frame = NSRect(origin: origin, size: size)
        
        let title = "\(subviewNumber++)"
        let view = CustomView.newCustomView()
        view.frame = frame
        view.label.stringValue = title
        contentView.addSubview(view)
    }
    
    func randomOriginInView(view: NSView, forViewOfSize size: CGSize) -> CGPoint {
        let widthScale = UInt32(CGRectGetWidth(view.bounds) - size.width)
        let heightScale = UInt32(CGRectGetHeight(view.bounds) - size.height)
        
        let x = CGFloat(arc4random_uniform(widthScale))
        let y = CGFloat(arc4random_uniform(heightScale))
        
        return CGPoint(x: x, y: y)
    }
}

